from os.path import join

import requests
from bs4 import BeautifulSoup
import re
#Install: pip install urltools
import urltools
#Install : pip install pymongo
import pymongo
import copy


def list_main(web_url, skip):
    url = web_url + "&skip=" + str(skip)
    print("Crawling website: " + url)
    code = requests.get(url)
    plain = code.text
    s = BeautifulSoup(plain, "html.parser")
    i = 0

    # create the list of companies
    companies_list = []
    for link in s.findAll('div', {'class': 'result'}):
        # print(link)
        aTag = link.find('a')
        i += 1
        company_name = link.find('a', {'class': 'companyname'}).text
        link = aTag.get('href')

        try:
            company = extract_url_from_details(link, company_name)
            company.print_info()
            companies_list.append(company)
            save_company_to_db(company, i+skip)
        except:
            continue

    # filtered_list = [elem for elem in companies_list if len(elem.instagram) > 1]
    #
    # save_list_to_file(filtered_list)

    # Pagination
    if i > 0:
        list_main(web_url, i + skip)


def extract_url_from_details(webPage, company_name):
    web_page = "https://zk.mk" + webPage
    code = requests.get(web_page)
    plain = code.text
    s = BeautifulSoup(plain, "html.parser")
    for link in s.findAll('a', {'class': 'website'}):
        web_link = link.get('href')
        try:
            return search_website_for_info(web_link, company_name)
        except AssertionError as error:
            # print(error)
            return


def search_website_for_info(web_page, company_name):
    if web_page == 'http://':
        return

    website = Firma()

    website.website = web_page
    website.name = company_name

    code = requests.get(web_page)
    plain = code.text
    soup = BeautifulSoup(plain, "html.parser")

    # Check if we can find the instagram info on first page
    insta_info = get_instagram_info(soup, plain)
    website.instagram = insta_info

    email_info = get_email_address(plain)
    website.email_list = email_info

    #Get all links from the starting page
    for href_tag in soup.findAll('a', attrs={'href': re.compile('^http')}):
        link = href_tag.get('href')

        #Extract the Domain so we don't search external Pages
        main_domain = urltools.parse(web_page).domain
        link_domain = urltools.parse(link).domain

        #Search for only pages:
        # -With the same Domain
        # -That are not the starting page ( we already searched this)
        # -That are not link to external images
        if main_domain == link_domain and link != web_page and not is_url_file(link):

            print('Searching Data on: ' + str(link))
            code = requests.get(link)
            plain = code.text
            soup = BeautifulSoup(plain, "html.parser")

            website.instagram.extend(get_instagram_info(soup,plain))

            website.email_list.extend(get_email_address(plain))
        else:
            print('SKIPPING Page : ' + str(link))

    return website


def get_instagram_info(soup_page,plain_page):

    insta_links = []

    for link in soup_page.findAll('a', attrs={'href': re.compile("^https://instagram")}):
        insta_links.append(link.get('href'))

    insta_links.extend(get_insta_info(plain_page))

    return insta_links


def get_insta_info(web_page_plain):
    # re.findall(r"\+\d{2}\s?0?\d{10}", s)
    insta_links = re.findall(r"instagram.com.\/[A-Za-z0-9.-]+", web_page_plain)
    return insta_links

def get_email_address(web_page_plain):

    # re.findall(r"\+\d{2}\s?0?\d{10}", s)
    email_list = re.findall(r"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", web_page_plain)
    return email_list


def is_url_file(web_page):
    if "jpg" in web_page:
        return True
    if "png" in web_page:
        return True
    if "gif" in web_page:
        return True
    if "pdf" in web_page:
        return True
    else:
        return False


class Firma:
    name = ""
    website = ""
    email = ""
    number = 0
    email_list = set()
    instagram = []

    def print_info(self):
        print("=====***======")
        print("Name\t" + self.name)
        print("Website\t" + self.website)
        print("Email List\t" + str(list(set(self.email_list))))
        print("Instagram\t" + str(list(set(self.instagram))))
        print("=====***======")

# def save_list_to_file(list):
#     myfile = open('./my_list.txt', mode='a+', encoding='utf-8')
#     for lines in list:
#         save_file(lines)
#     myfile.close()


def save_file(company,number):
    myfile = open('./my_list.txt', mode='a')
    myfile.write("\nNumber:\t#" + str(number))
    myfile.write("\n=====***======")
    myfile.write("\nName\t" + company.name.encode('utf-8').strip())
    myfile.write("\nWebsite\t" + company.website.encode('utf-8').strip())
    myfile.write("\nEmail List\t")
    for item in set(company.email_list):
        myfile.write("{}\t".format(item))
    myfile.write("\nInstagram\t")
    for item in set(company.instagram):
        myfile.write("{}\t".format(item))
    myfile.write("\n=====***======")
    myfile.write('\n')
    myfile.close()


def save_company_to_db(company,number):
    myclient = pymongo.MongoClient("mongodb://admin:ddiIJFeaeRbRk0jZxn1krlQB@mongodb.back4app.com:27017/f63b98547e2d41b9b984eb52573a242c?ssl=true")
    mydb = myclient["f63b98547e2d41b9b984eb52573a242c"]
    collection = mydb["TuristickiAgencii"]

    insert_list = []
    for email in set(company.email_list):
        print("Email: " + email)
        insert_obj = Firma()
        insert_obj.email = email
        insert_obj.number = number
        insert_obj.email_list = company.email_list
        insert_obj.instagram = company.instagram
        insert_obj.website = company.website
        insert_obj.name = company.name
        insert_list.append(insert_obj.__dict__)

    collection.insert_many(insert_list)

#Start the script
list_main('https://zk.mk/search/?what=turisticka+agencija', 0)
#save_company_to_db()